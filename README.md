Requirements
------------
- npm
- nodejs

Installation
------------
- Open terminal
- Clone the repository `git clone https://albemumo@bitbucket.org/albemumo/hoyvoy-frontend.git`
- Go to the project root. `cd hoyvoy-frontend`
- Execute `npm install` command.

Running local development server
--------------------------------
- Inside project root folder, execute server: `npm run serve`
- Access url `http://localhost:8080/` from browser


External libraries installed
----------------------------

- axios/axios https://github.com/axios/axios
Promise based HTTP client for the browser and node.js.
